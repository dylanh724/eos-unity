// Copyright Epic Games, Inc. All Rights Reserved.
// This file is automatically generated. Changes to this file may be overwritten.

using System;
using System.Runtime.InteropServices;

namespace Epic.OnlineServices.Presence
{
	/// <summary>
	/// Function prototype definition for notifications that come from <see cref="PresenceInterface.AddNotifyJoinGameAccepted" />
	/// </summary>
	/// <param name="data">A <see cref="JoinGameAcceptedCallbackInfo" /> containing the output information and result</param>
	public delegate void OnJoinGameAcceptedCallback(JoinGameAcceptedCallbackInfo data);

	internal delegate void OnJoinGameAcceptedCallbackInternal(IntPtr messagePtr);
}