﻿using UnityEngine;
using Epic.OnlineServices.Auth;

// More-Efficient Coroutines
using MEC;
using IEnumerator = System.Collections.Generic.IEnumerator<float>;

/// <summary>
/// Epic Online Services (EOS) Manager : Login
/// https://dev.epicgames.com/docs/services/en-US/CSharp/GettingStarted/index.html
/// </summary>
public static class EOSLogin
{
    #region Vars
    public static EOSMgr s_eosMgr => EOSMgr.s_EosMgr;
    public static Epic.OnlineServices.Platform.PlatformInterface s_platform => EOSMgr.s_Platform;
    public const LoginCredentialType LOGIN_TYPE = LoginCredentialType.AccountPortal;
    #endregion /Vars

    // ....................................................................................
    /// <summary>Based off LOGIN_TYPE</summary>
    private class LoginIdToken
    {
        public string Id
        {
            get
            {
                switch (LOGIN_TYPE)
                {
                    case LoginCredentialType.AccountPortal:
                        return "";
                    
                    // TODO: Handle other login types
                    default: 
                        return "";
                }
            }
        }

        public string Token
        {
            get
            {
                switch (LOGIN_TYPE)
                {
                    case LoginCredentialType.AccountPortal:
                        return "";

                    // TODO: Handle other login types
                    default:
                        return "";
                }
            }
        }
    }

    // ....................................................................................
    /// <summary>Async func => Calls back success bool after successful login @ loginAsync()</summary>
    /// <param name="isSuccessCallback"></param>
    public static IEnumerator _PrepLogin()
    {
        //Debug.Log("[EOSLogin] @ _PrepLogin");

        var authInterface = s_platform.GetAuthInterface();
        
        // Validate authInterface + credentials
        if (authInterface == null)
        {
            Debug.LogError("[EOSLogin]**ERR @ _prepLogin: Failed to get auth interface");
            s_eosMgr.StopInit = true;
            yield break;
        }

        // Ensure credentials are set correctly (!TODO/null). You did check out the README, right? ;)
        if (!EOSInit.CheckValidCredentials())
        {
            Debug.LogError("[EOSLogin]**ERR @ _prepLogin: Invalid credentials >> " +
                "Replace the 'TODO' or empty vars @ /Assets/_Scripts/EOS/EOSInit.cs (@ the top)");
            s_eosMgr.StopInit = true;
            yield break;
        }

        // -------------------------
        // Ready to attempt a login after gathering credentials
        Credentials credentials = getDynamicCredentials();

        var loginOptions = new LoginOptions()
        {
            Credentials = credentials,
            ScopeFlags = AuthScopeFlags.BasicProfile | AuthScopeFlags.FriendsList | AuthScopeFlags.Presence
        };

        loginAsync(authInterface, loginOptions); // Login is async with callback =>
    }

    // ....................................................................................
    private static Credentials getDynamicCredentials()
    {
        var LoginIdToken = new LoginIdToken();
        
        return new Credentials()
        {
            // ===========================
            // Dynamic based on login type
            Type = LOGIN_TYPE,
            Id = LoginIdToken.Id,
            Token = LoginIdToken.Token

            // ===========================
            // email + pass
            //Type = LoginCredentialType.Password
            //Id = "<email>",
            //Token = "<pass>",

            // ===========================
            //Type = LoginCredentialType.ExchangeCode // ?
            //Type = LoginCredentialType.PersistentAuth // After 1 successful login, can auto-login via this?
            //Type = LoginCredentialType.RefreshToken // ?

            // ===========================
            // AccountPortal: No id/token. This will launch a login page and callback.
            //Type = LoginCredentialType.AccountPortal

            // ===========================
            // Dev id is "localhost:somePortNum" // token is username entered (not the token below it...?)
            //Type = LoginCredentialType.Developer
            //Id = "localhost:51515",
            //Token = "xbdev",
        };
    }

    // ....................................................................................
    private static void loginAsync(AuthInterface auth, LoginOptions loginOpts)
    {
        Debug.Log("[EOSLogin] @ loginAsync");

        System.Object clientData = null; // TODO: Pass aribtrary data to login callback
        auth.Login(loginOpts, clientData, (LoginCallbackInfo callbackInfo) =>
        {
            s_eosMgr.IsLoggedIn = callbackInfo.ResultCode == Epic.OnlineServices.Result.Success;

            if (!(bool)s_eosMgr.IsLoggedIn)
                onLoginFail(callbackInfo);
            else
                s_eosMgr.OnLoginSuccess(callbackInfo);
        });
        
        // *Remember, Login() is async => Anything below here will happen BEFORE the login callback.
    }

    // ....................................................................................
    /// <summary>
    /// Tried to login, but something went wrong. Check the ResultCode for specifics.
    /// </summary>
    /// <param name="_callbackInfo"></param>
    private static void onLoginFail(LoginCallbackInfo _callbackInfo)
    {
        Debug.LogError($"[EOSLogin]**ERR @ onLoginFail => callback ResultCode=={_callbackInfo.ResultCode.ToString()}");
        s_eosMgr.StopInit = true;

        // TODO: Handle fail
    }

}
